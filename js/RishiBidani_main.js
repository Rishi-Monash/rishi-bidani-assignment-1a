"use strict";
/* ===========================
NAME: Rishi Bidani
ID: 31883125
DATE: 29-06-21
===============================  */
console.log("Rishi Bidani Assignment 1a");

let bookingData = [
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    {
        time: "08:00",
        reason: "",
        label: "",
        booked: false,
    },
    {
        time: "09:00",
        reason: "",
        label: "",
        booked: false,
    },
    {
        time: "10:00",
        reason: "",
        label: "",
        booked: false,
    },
    {
        time: "11:00",
        reason: "",
        label: "",
        booked: false,
    },
    {
        time: "12:00",
        reason: "",
        label: "",
        booked: false,
    },
    {
        time: "13:00",
        reason: "",
        label: "",
        booked: false,
    },
    {
        time: "14:00",
        reason: "",
        label: "",
        booked: false,
    },
    {
        time: "15:00",
        reason: "",
        label: "",
        booked: false,
    },
    {
        time: "16:00",
        reason: "",
        label: "",
        booked: false,
    },
    {
        time: "17:00",
        reason: "",
        label: "",
        booked: false,
    },
];
// Important Variables
const timeNow = document.querySelector("#timeNow");
const output = document.querySelector("#output");
const bookRoomButton = document.querySelector("#bookRoom");
const clearBookBut = document.querySelector("#clearBookings");

// TASK 1 Function
/** ═══════════════════════════════════════════════════════
 * Update the booking data for the room
 * @param time = time slot
 * @param reason = reason for booking
 * @param label = this will show up as text near the time slot
 ═══════════════════════════════════════════════════════════ **/
function bookRoom(time, reason, label) {
    for (let i = 0; i < bookingData.length; i++) {
        if (typeof bookingData[i] === "object" && bookingData[i].time === time) {
            bookingData[i].reason = reason;
            bookingData[i].label = label;
            bookingData[i].booked = true;
        } else {
        }
    }
}

// Task 2 Function
/** ═══════════════════════════════════════════════════════
 * Returns booked status of time slot
 * @param time = Time slot
 * @return {boolean} = time slot is available or not
 * @return {string} = will return string in case of error
 ═══════════════════════════════════════════════════════════ **/
function checkRoomBooked(time) {
    const searchArr = bookingData.find((elem) => {
        if (typeof elem === "object" && elem.time === time) {
            return elem;
        }
    });
    return searchArr.booked;
}

// Task 3 Function
/** ═══════════════════════════════════════════════════════
 * This will reset all the properties of the original data
 ═══════════════════════════════════════════════════════════ **/
function clearRoomBookings() {
    for (let i = 0; i < bookingData.length; i++) {
        if (typeof bookingData[i] === "object") {
            bookingData[i].reason = "";
            bookingData[i].label = "";
            bookingData[i].booked = false;
        }
    }
}

// TASK 4 = HTML

// TASK 5
/** ═══════════════════════════════════════════════════════
 * This function will just update the bookings for the
 * different time slots available.
 ═══════════════════════════════════════════════════════════ **/
function updateDisplay() {
    // First Reset, then print the time slots
    output.innerHTML = "";
    for (let i = 0; i < bookingData.length; i++) {
        // Check if current element is an object
        if (typeof bookingData[i] === "object") {
            const p = document.createElement("p");
            if (!checkRoomBooked(bookingData[i].time)) {
                p.innerText = `${bookingData[i].time} (Available)`;
                p.classList.add("form-content");
                output.appendChild(p);
            } else if (checkRoomBooked(bookingData[i].time)) {
                p.innerText = `${bookingData[i].time} NOT AVAILABLE - ${bookingData[i].label}`;
                p.classList.add("form-content");
                output.appendChild(p);
            }
        }
    }
}

// TASK 6
/** ═══════════════════════════════════════════════════════
 * This function will call bookRoom and updateDisplay to book a room
 ═══════════════════════════════════════════════════════════ **/
function doBookings() {
    // window.event.preventDefault() // for onclick - since I'm using a form tag
    const startTime = document.querySelector("#inputTime").value;
    const inpReason = document.querySelector("#inputReason").value;
    const inpLabel = document.querySelector("#inputLabel").value;
    if (startTime.length !== 0 && inpReason.length !== 0 && inpLabel.length !== 0) {
        if (!checkRoomBooked(startTime)) {
            bookRoom(startTime, inpReason, inpLabel);
        } else {
            alert("Room Booked !");
        }
        updateDisplay();
    } else {
        alert("Fill All The Fields");
    }
}

bookRoomButton.addEventListener("click", (event) => {
    // event.preventDefault()
    doBookings();
});

// TASK 7
// This task will confirm with the user if they wish to clear the bookings
clearBookBut.addEventListener("click", () => {
    const confirmClear = confirm("Are you sure you wish to clear all the  bookings");
    if (confirmClear) {
        // Confirmed
        clearRoomBookings();
        updateDisplay();
    } else {
        // Cancelled
    }
});

// TASK 8
// This function will run automatically without window.onload
// However, I could use it as follow
// window.onload = ()=>{restOfTheCode}
// Show time instantly without having to wait for 1 second.
timeNow.innerText = new Date().toLocaleTimeString();
/** ═══════════════════════════════════════════════════════
 * Update the time every second, using a recursive function
 * @returns {string} = updates the time in #timeNow
 ═══════════════════════════════════════════════════════════ **/
function updateDayTime() {
    setTimeout(() => {
        timeNow.innerText = updateDayTime();
    }, 1000);
    return new Date().toLocaleTimeString();
}

updateDisplay();
updateDayTime();
